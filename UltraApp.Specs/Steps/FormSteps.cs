using FluentAssertions;
using TechTalk.SpecFlow;
using UltraApp.Specs.Drivers;
using UltraApp.Specs.PageObjects;

namespace UltraApp.Specs.Steps
{
    [Binding]
    public class FormSteps
    {
        //Page Object for Calculator
        private readonly FormPageObject _formPageObject;

        public FormSteps(BrowserDriver browserDriver)
        {
            _formPageObject = new FormPageObject(browserDriver.Current);
        }
        
        [Given(@"user is on Form page")]
        public void GivenUserIsOnFormPage()
        {
            _formPageObject.EnsureFormPageIsOpen();
        }

        [When(@"the button is clicked")]
        public void WhenTheButtonIsClicked()
        {
            _formPageObject.ClickButton();
        }

        [Then(@"input value is ""(.*)""")]
        public void ThenInputValueIs(string p0)
        {
            var result =  _formPageObject.WaitForNonEmptyResult();
            result.Should().Be(p0);
        }
    }
}