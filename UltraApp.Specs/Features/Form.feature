Feature: Form
	Pressing button sets the value of input

Scenario: Click button
	Given user is on Form page
	When the button is clicked
	Then input value is "John Travolta"