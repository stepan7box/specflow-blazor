using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using UltraApp.Specs.Helpers;

namespace UltraApp.Specs.PageObjects
{
    public class FormPageObject
    {
        private string FormPageUrl = $"{EnvironmentHelper.BaseUrl}/form";
        
        //The Selenium web driver to automate the browser
        private readonly IWebDriver _webDriver;
        
        //Finding elements by ID
        private IWebElement InputElement => _webDriver.FindElement(By.CssSelector("[data-qa=\"form-name\"]"));
        private IWebElement ButtonSetNameElement => _webDriver.FindElement(By.CssSelector("[data-qa=\"btn-set-name\"]"));
        
        //The default wait time in seconds for wait.Until
        public const int DefaultWaitInSeconds = 5;

        public FormPageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void ClickButton()
        {
            ButtonSetNameElement.Click();
        }
        
        public void EnsureFormPageIsOpen()
        {
            //Open the form page in the browser if not opened yet
            if (_webDriver.Url != FormPageUrl)
            {
                _webDriver.Url = FormPageUrl;
            }
            //Otherwise reset the calculator by clicking the reset button
            else
            {
                //Click the rest button
                InputElement.Click();

                //Wait until the result is empty again
                WaitForEmptyResult();
            }

        }
            
        public string WaitForNonEmptyResult()
        {
            //Wait for the result to be not empty
            return WaitUntil(
                () => InputElement.GetAttribute("value"),
                result => !string.IsNullOrEmpty(result));
        }

        public string WaitForEmptyResult()
        {
            //Wait for the result to be empty
            return WaitUntil(
                () => InputElement.GetAttribute("value"),
                result => result == string.Empty);
        }
        
        /// <summary>
        /// Helper method to wait until the expected result is available on the UI
        /// </summary>
        /// <typeparam name="T">The type of result to retrieve</typeparam>
        /// <param name="getResult">The function to poll the result from the UI</param>
        /// <param name="isResultAccepted">The function to decide if the polled result is accepted</param>
        /// <returns>An accepted result returned from the UI. If the UI does not return an accepted result within the timeout an exception is thrown.</returns>
        private T WaitUntil<T>(Func<T> getResult, Func<T, bool> isResultAccepted) where T: class
        {
            var wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(DefaultWaitInSeconds));
            return wait.Until(driver =>
            {
                var result = getResult();
                if (!isResultAccepted(result))
                    return default;

                return result;
            });
        }

    }
}